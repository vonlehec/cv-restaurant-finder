# Noteworth Lunch


This app utilizes the google places API to display restaurants near a location that the user specifies. They are also able to sort the results by distance, number of reviews, and best match. When they tap on a restaurant, they are taken to the restaurant's details. Other features include, but or not limited to:

  - View the ratings of the different restaurants
  - View an image of the restaurant and tap to enlarge that image
  - Calling a restaurant (if the user's device can make phone calls)
  - Navigating to the restaurant

# Front-End Development

  - This app focuses on front-end development, using Google Places API
  - The app downloads all necessary data and loads it into an MVC design pattern

# Original Code
All code was written by me, Charles von Lehe, except for 3rd party Cocoapods. These Cocoapods are listed below:

  - Alamofire
  - DropDown
  - JTSImageViewController
  - HCSStarRatingView
  - LocationPickerViewController
  - JGProgressHUD

# Architecture
The app loads all details of each individual restaurant from the API when the user changes the parameters of their search. This is necessary since Google's places API does not have the ability to retrieve the number of reviews without retrieving the details. The two trade-offs to this are that it makes numerous calls to the API and that the Google API only allows for up to 5 reviews to be retrieved.

Having more time, I would load the places only when changing the location, and sort those on the fron-end, thus reducing the number of calls made to the database. Although the current solution services the desired purpose, it could be made to be more efficient by scaling back on calls to the Google API.

# Other code

Another app that I am particularly proud of is Historic Walking Tour. It is very efficient and has an aesthetically-pleasing UI. It is an app that displays Historical Sites in given cities and plays audio for each site. This code can be found on Bitbucket at the following URL:  https://vonlehec@bitbucket.org/vonlehec/hwt-sb.git.



# About Me
* [Resume](http://docdro.id/T2vHCnS)
* [LinkedIn](https://www.linkedin.com/in/charles-von-lehe-63410a3a/)

