//
//  PlaceTableViewCell.swift
//  CV Restaurant Finder
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit
import HCSStarRatingView

class PlaceTableViewCell: UITableViewCell {
   @IBOutlet weak var placeImageView: UIImageView!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var ratingContainerView: UIView!
   var ratingView:HCSStarRatingView!
   @IBOutlet weak var addressLabel: UILabel!
   @IBOutlet weak var numberOfReviewsLabel: UILabel!
   @IBOutlet weak var distanceLabel: UILabel!
   
   override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
   func populate (forPlace:Place) {
      setReviewsLabel(forPlace: forPlace)
      setDistanceLabel(forPlace: forPlace)
      setRating(forPlace: forPlace)
      setImage(forPlace: forPlace)
      titleLabel.text = forPlace.name
      addressLabel.text = forPlace.address
   }
   
   private func setReviewsLabel (forPlace:Place) {
      let nf = NumberFormatter()
      nf.numberStyle = .decimal
      numberOfReviewsLabel.text = nf.string(from: NSNumber(value: forPlace.numberOfReviews))! + " Reviews"
   }
   
   private func setDistanceLabel (forPlace:Place) {
      if let distance = forPlace.distance {
         distanceLabel.text = String(format: "%.2f", distance / 1609.3435021075907) + " mi"
         
      }else {
         distanceLabel.text = ""
      }
   }
   
   private func setImage (forPlace:Place) {
      placeImageView.layer.cornerRadius = 4.0
      forPlace.getImage { (image) in
         if let image = image {
            self.placeImageView.image = image
         }else {
            self.placeImageView.image = #imageLiteral(resourceName: "placeholder")
         }
      }
   }
   
   private func setRating (forPlace:Place) {
      if ratingView == nil {
         ratingView = HCSStarRatingView(frame: CGRect(x: 0, y: 0, width: ratingContainerView.frame.size.width, height: ratingContainerView.frame.size.height))
         ratingView.isUserInteractionEnabled = false
         ratingView.allowsHalfStars = true
         ratingView.accurateHalfStars = true
         ratingContainerView.addSubview(ratingView)
         ratingView.constrainToSuperView()
      }
      if let rating = forPlace.rating {
         ratingView.value = CGFloat(rating)
      }else {
         ratingView.value = 0
      }
   }

   override func prepareForReuse() {
      super.prepareForReuse()
      titleLabel.text = nil
      placeImageView.image = #imageLiteral(resourceName: "placeholder")
   }
}
