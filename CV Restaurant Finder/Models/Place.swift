//
//  Place.swift
//  CV Restaurant Finder
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire

class Place {
   var location:CLLocation?
   private var photoReference:String?
   private var image:UIImage?
   var name:String?
   var placeId:String?
   var address:String?
   var phoneNumber:String?
   var rating:Float?
   var numberOfReviews = 0
   var website:String?
   var distance:Double?
   
   init(dictionary:[String:Any]) {
      if
         let geometry = dictionary[GoogleAPI.geometry] as? [String:Any],
         let locDict = geometry[GoogleAPI.location] as? [String:Any],
         let lat = locDict[GoogleAPI.lat] as? Double,
         let lng = locDict[GoogleAPI.lng] as? Double {
         location = CLLocation(latitude: lat, longitude: lng)
         CLLocation.getCurrent(completion: { (currentLocation) in
            self.distance = currentLocation?.distance(from: self.location!)
         })
      }
      if
         let photos = dictionary[GoogleAPI.photos] as? [[String:Any]],
         let photo = photos.first,
         let photoRef = photo[GoogleAPI.photo_reference] as? String {
         photoReference = photoRef
      }
      name = dictionary[GoogleAPI.name] as? String
      placeId = dictionary[GoogleAPI.place_id] as? String

   }
   
   func setDetails(forDict:[String:Any]) {
      address = forDict[GoogleAPI.formatted_address] as? String
      phoneNumber = forDict[GoogleAPI.formatted_phone_number] as? String
      rating = forDict[GoogleAPI.rating] as? Float
      if let reviews = forDict[GoogleAPI.reviews] as? [Any] {
         numberOfReviews = reviews.count
      }
      website = forDict[GoogleAPI.website] as? String
   }
   
   class func get(sortByDistance:Bool, forLocation:CLLocation?, forType:String?, completion:@escaping (_ places:[Place])->Void) {
      GoogleAPIHelper.get(sortByDistance:sortByDistance, forType:forType, forLocation: forLocation, completion: { (places) in
         completion(places)
      })
   }
   
   func getImage(completion:@escaping (_ image:UIImage?)->Void) {
      if let image = image {
         completion(image)
         return
      }
      guard let photoRef = photoReference, let url = URL(string: GoogleAPI.photoUrl) else {
         completion(nil)
         return
      }
      
      let params = [GoogleAPI.photoreference:photoRef, GoogleAPI.sensor:false, GoogleAPI.maxheight: 800, GoogleAPI.maxwidth:800, GoogleAPI.key: GoogleAPI.apiKey] as [String : Any]
      Alamofire.request(url, method: .get, parameters: params).responseData { (response) in
         if response.result.isSuccess, let data = response.data {
            self.image = UIImage(data: data)
            completion(self.image)
         }else {
            completion(nil)
         }
      }
   }
}
