//
//  MainViewController.swift
//  CV Restaurant Finder
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation
import LocationPickerViewController

class MainViewController: UIViewController {
   @IBOutlet weak var tableView: UITableView!
   var places:[Place] = []
   @IBOutlet weak var typeLabel: UILabel!
   var dropDown:DropDown!
   var selectedLocation:CLLocation?
   var selectedType:String?
   @IBOutlet weak var searchBar: UISearchBar!
   @IBOutlet weak var segmentedControl: UISegmentedControl!
   
   override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.gotLocation), name: NSNotification.Name(rawValue: NotificationNames.gotLocation), object: nil)
      setupTypeLabel()
    }
   
   func setupTypeLabel () {
      typeLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MainViewController.typeLabelTapped(_:))))
      dropDown = DropDown(anchorView: typeLabel)
      dropDown.dataSource = restaurantTypes
      dropDown.selectionAction = {(index: Int, item: String) in
         self.typeLabel.text = item + " ▼"
         if item == "All" {
            self.selectedType = nil
         }else {
            self.selectedType = item
         }
         self.getPlaces()
      }
   }
   
   @objc func typeLabelTapped(_ tap:UITapGestureRecognizer) {
      dropDown.show()
   }
   
   @objc func gotLocation() {
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationNames.gotLocation), object: nil)
      CLLocation.getCurrent { (location) in
         self.selectedLocation = location
         self.getPlaces()
      }
   }
   
   func getPlaces () {
      MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
      Place.get(sortByDistance:(segmentedControl.selectedSegmentIndex != 1), forLocation: selectedLocation, forType: selectedType) { (places) in
         if self.segmentedControl.selectedSegmentIndex == 2 {
            self.places = places.sorted(by: { (place1, place2) -> Bool in
               return place1.numberOfReviews > place2.numberOfReviews
            })
         }else {
            self.places = places
         }
         self.tableView.reloadData()
         MethodHelper.hideHUD()
      }
   }
   
   override var preferredStatusBarStyle: UIStatusBarStyle {
      return UIStatusBarStyle.lightContent
   }

   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
   }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   @IBAction func selectedSegment(_ sender: UISegmentedControl) {
      getPlaces()
   }
   
}

extension MainViewController:UITableViewDataSource, UITableViewDelegate {
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return places.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.PlaceTableViewCell, for: indexPath) as? PlaceTableViewCell else {return UITableViewCell()}
      cell.populate(forPlace: places[indexPath.row])
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      DetailsViewController.display(viewController: self, forPlace: places[indexPath.row])
   }
}

extension MainViewController:UISearchBarDelegate {
   func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
      displayLocationPicker()
      return false
   }
   
   func displayLocationPicker () {
      let locationPicker = LocationPicker()
      locationPicker.pickCompletion = { (pickedLocationItem) in
         // Do something with the location the user picked.
         guard let coordinates = pickedLocationItem.coordinate else {
            return
         }
         self.selectedLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
         self.searchBar.text = pickedLocationItem.name
         self.getPlaces()
         
      }
      locationPicker.addBarButtons()
      // Call this method to add a done and a cancel button to navigation bar.
      
      let navigationController = UINavigationController(rootViewController: locationPicker)
      present(navigationController, animated: true, completion: nil)
   }
}
