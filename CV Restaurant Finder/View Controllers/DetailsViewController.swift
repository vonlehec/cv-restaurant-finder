//
//  DetailsViewController.swift
//  CV Restaurant Finder
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit
import MapKit
import HCSStarRatingView

class DetailsViewController: UIViewController {
   var place:Place!
   @IBOutlet weak var imageView: UIImageView!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var ratingContainerView: UIView!
   @IBOutlet weak var distanceLabel: UILabel!
   @IBOutlet weak var reviewsLabel: UILabel!
   @IBOutlet weak var addressLabel: UILabel!
   @IBOutlet weak var callButton: UIButton!
   @IBOutlet weak var navigateButton: UIButton!
   @IBOutlet weak var websiteButton: UIButton!
   @IBOutlet weak var mapView: MKMapView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      callButton.layer.cornerRadius = 4.0
      navigateButton.layer.cornerRadius = 4.0
      websiteButton.layer.cornerRadius = 4.0
      populateLabels()
      setImage()
      addRatingView()
      populateMap()
    }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      imageView.addBlackGradientLayer(frame: imageView.frame)
   }
   func populateMap () {
      guard let location = place.location else {return}
      let annotation = MKPointAnnotation()
      annotation.coordinate = location.coordinate
      annotation.title = place.name
      mapView.addAnnotation(annotation)
      mapView.showAnnotations([annotation], animated: true)
   }
   
   func addRatingView () {
      let ratingView = HCSStarRatingView(frame: CGRect(x: 0, y: 0, width: ratingContainerView.frame.size.width, height: ratingContainerView.frame.size.height))
      ratingView.isUserInteractionEnabled = false
      ratingView.allowsHalfStars = true
      ratingView.accurateHalfStars = true
      ratingContainerView.addSubview(ratingView)
      ratingView.constrainToSuperView()
      
      if let rating = place.rating {
         ratingView.value = CGFloat(rating)
      }else {
         ratingView.value = 0
      }
   }
   
   private func setReviewsLabel () {
      let nf = NumberFormatter()
      nf.numberStyle = .decimal
      reviewsLabel.text = nf.string(from: NSNumber(value: place.numberOfReviews))! + " Reviews"
   }
   
   private func setDistanceLabel () {
      if let distance = place.distance {
         distanceLabel.text = String(format: "%.2f", distance / 1609.3435021075907) + " mi"
         
      }else {
         distanceLabel.text = ""
      }
   }
   
   private func setImage () {
      imageView.layer.cornerRadius = 4.0
      place.getImage { (image) in
         if let image = image {
            self.imageView.image = image
            self.imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DetailsViewController.imageViewTapped(_:))))
            
         }else {
            self.imageView.image = #imageLiteral(resourceName: "placeholder")
         }
      }
   }
   
   @objc func imageViewTapped (_ tap:UITapGestureRecognizer) {
      imageView.displayImage(viewController: self)
   }
   
   func populateLabels () {
      setReviewsLabel()
      setDistanceLabel()
      titleLabel.text = place.name
      addressLabel.text = place.address
   }
   
   override var preferredStatusBarStyle: UIStatusBarStyle {
      return UIStatusBarStyle.lightContent
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
   @IBAction func backButtonPressed (_ sender:UIButton) {
      close()
   }
   @IBAction func callButtonPressed(_ sender: UIButton) {
      guard let phoneNumber = place.phoneNumber else {return}
      if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
         UIApplication.shared.open(url)
      }
   }
   
   @IBAction func navigateButtonPressed(_ sender: UIButton) {
      guard let coordinates = place.location?.coordinate else {return}
      let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
      let mapItem = MKMapItem(placemark: placemark)
      mapItem.name = place.name
      mapItem.openInMaps(launchOptions: nil)
   }
   
   @IBAction func websiteButtonPressed(_ sender: UIButton) {
      guard let website = place.website else {return}
      if let url = URL(string: website), UIApplication.shared.canOpenURL(url) {
         UIApplication.shared.open(url)
      }
   }
   
   
   class func display(viewController:UIViewController, forPlace:Place) {
      guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.DetailsViewController) as? DetailsViewController else {return}
      nextView.place = forPlace
      viewController.present(nextView, animated: true, completion: nil)
   }

}
