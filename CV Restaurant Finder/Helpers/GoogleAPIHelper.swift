//
//  GoogleAPIHelper.swift
//  CV Restaurant Finder
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

class GoogleAPIHelper {
   class func get (sortByDistance:Bool, forType:String?, forLocation:CLLocation?, completion:@escaping (_ places:[Place])->Void) {
      guard let location = forLocation, let url = URL(string: GoogleAPI.placeSearchUrl) else {
         completion([])
         return}
      var params:[String:Any] = [
         GoogleAPI.key:GoogleAPI.apiKey,
         GoogleAPI.location:"\(location.coordinate.latitude),\(location.coordinate.longitude)",
         GoogleAPI.type:GoogleAPI.restaurant]
      if let type = forType {
         params[GoogleAPI.keyword] = type
      }
      if sortByDistance {
         params[GoogleAPI.rankby] = GoogleAPI.distance
      }else {
         params[GoogleAPI.radius] = 50000
         params[GoogleAPI.rankby] = GoogleAPI.prominence
      }
      Alamofire.request(url, method: .get, parameters: params).responseJSON { (response) in
         guard let results = self.getDictionary(fromData: response.data)?[GoogleAPI.results] as? [[String:Any]] else {
            completion([])
            return
         }
         var places:[Place] = []
         for result in results {
            places.append(Place(dictionary: result))
         }
         let numberOfTasks = places.count
         var completedTasks = 0
         
         for place in places {
            self.getDetails(forPlace: place, completion: { (success) in
               completedTasks += 1
               if numberOfTasks == completedTasks {
                  completion(places)
               }
            })
         }
      }
   }
   
   class func getDetails (forPlace:Place, completion:@escaping (_ success:Bool)->Void) {
      guard let placeId = forPlace.placeId, let url = URL(string:GoogleAPI.placeDetailsUrl) else {
         completion(false)
         return
      }
      let params = [GoogleAPI.key:GoogleAPI.apiKey, GoogleAPI.placeid:placeId]
      Alamofire.request(url, method: .get, parameters: params).responseJSON { (response) in
         guard let result = self.getDictionary(fromData: response.data)?[GoogleAPI.result] as? [String:Any] else {
            completion(false)
            return
         }
         forPlace.setDetails(forDict: result)
         completion(true)
      }
   }
   
   private class func getDictionary (fromData:Data?)->[String:Any]? {
      guard let data = fromData else {return nil}
      do {
         let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
         return json as? [String:Any]
      }catch {
         return nil
      }
   }
}
