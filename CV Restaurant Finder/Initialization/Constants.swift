//
//  Constants.swift
//  CV Restaurant Finder
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation
import UIKit

let restaurantTypes = ["All","Mexican","Italian","Indian","Cajun","Thai","Greek","Chinese","Mediterranean","French","Spanish","Caribbean","Fast Food"].sorted()
struct Colors {
   static let primary = #colorLiteral(red: 0.9843137255, green: 0.768627451, blue: 0.1176470588, alpha: 1)
   static let primaryLight = #colorLiteral(red: 0.9960784314, green: 0.8823529412, blue: 0.5294117647, alpha: 1)
   static let primaryDark = #colorLiteral(red: 0.1490196078, green: 0.231372549, blue: 0.4784313725, alpha: 1)
}

struct ViewControllerIDs {
   static let DetailsViewController = "DetailsViewController"
   static let MainViewController = "MainViewController"
}

struct GoogleAPI {
   static let placeSearchUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
   static let placeDetailsUrl = "https://maps.googleapis.com/maps/api/place/details/json"
   static let photoUrl = "https://maps.googleapis.com/maps/api/place/photo"
   static let apiKey = "AIzaSyAp8zPuTFaEtNpbeQ56tFOau0LlbYKJbZI"
   static let key = "key"
   static let location = "location"
   static let rankby = "rankby"
   static let distance = "distance"
   static let type = "type"
   static let restaurant = "restaurant"
   static let results = "results"
   static let geometry = "geometry"
   static let lat = "lat"
   static let lng = "lng"
   static let photos = "photos"
   static let photo_reference = "photo_reference"
   static let name = "name"
   static let place_id = "place_id"
   static let placeid = "placeid"
   static let result = "result"
   static let photoreference = "photoreference"
   static let sensor = "sensor"
   static let maxheight = "maxheight"
   static let maxwidth = "maxwidth"
   static let formatted_address = "formatted_address"
   static let formatted_phone_number = "formatted_phone_number"
   static let rating = "rating"
   static let reviews = "reviews"
   static let website = "website"
   static let keyword = "keyword"
   static let prominence = "prominence"
   static let radius = "radius"
}

struct CellIDs {
   static let PlaceTableViewCell = "PlaceTableViewCell"
}

struct NotificationNames {
   static let gotLocation = "gotLocation"
}
