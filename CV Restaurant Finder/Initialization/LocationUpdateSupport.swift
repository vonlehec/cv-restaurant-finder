//
//  LocationUpdateSupport.swift
//  Historic Walking Tour
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

extension AppDelegate:CLLocationManagerDelegate {
    func setupLocationManager () {
        locationManager = CLLocationManager()
        //set delegate
        locationManager.delegate = self
        // This is the most important property to set for the manager. It ultimately determines how the manager will
        // attempt to acquire location and thus, the amount of power that will be consumed.
        
        locationManager.desiredAccuracy = 45
        locationManager.distanceFilter = 100
        // Once configured, the location manager must be "started".
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {return}
        currentLocation = location
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.gotLocation), object: nil)
    }
}
