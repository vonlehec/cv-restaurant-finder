//
//  Extensions.swift
//  CV Restaurant Finder
//
//  Created by Charlie  on 9/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import JTSImageViewController
import JGProgressHUD

extension UIAlertController {
   class func display (title:String, message:String, viewController:UIViewController) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
      viewController.present(alertController, animated: true, completion: nil)
   }
}

extension UIViewController {
   func close () {
      if let navController = navigationController {
         navController.popViewController(animated: true)
      }else {
         dismiss(animated: true, completion: nil)
      }
   }
}

extension CLLocation {
   func openInMaps(locationTitle:String?) {
      
      
      let regionDistance:CLLocationDistance = 10000
      let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate, regionDistance, regionDistance)
      let options = [
         MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
         MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
      ]
      let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
      let mapItem = MKMapItem(placemark: placemark)
      if let name = locationTitle {
         mapItem.name = name
      }
      mapItem.openInMaps(launchOptions: options)
   }
   
   class func getCurrent (completion:@escaping (_ locationO:CLLocation?)->Void) {
      completion((UIApplication.shared.delegate as? AppDelegate)?.currentLocation)
   }
}

extension UIImageView{
   func addBlackGradientLayer(frame: CGRect){
      let gradient = CAGradientLayer()
      gradient.frame = frame
      gradient.colors = [ UIColor.clear.cgColor, UIColor.black.cgColor]
      gradient.locations = [0.5, 1]
      self.layer.addSublayer(gradient)
   }
   
   func displayImage (viewController:UIViewController) {
      guard let img = image else {return}
      let imageInfo = JTSImageInfo()
      imageInfo.image = img
      imageInfo.referenceRect = frame
      imageInfo.referenceView = viewController.view
      let imageView = JTSImageViewController(imageInfo: imageInfo, mode: .image, backgroundStyle: .blurred)
      imageView?.show(from: viewController, transition: .fromOriginalPosition)
   }
   
   
}

extension UIView {
   func constrainToSuperView() {
      if superview == nil {
         return
      }
      
      guard let superview = self.superview else {
         return
      }
      
      self.translatesAutoresizingMaskIntoConstraints = false
      let x = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1, constant: 0)
      let y = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1, constant: 0)
      let w = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: superview, attribute: .width, multiplier: 1, constant: 0)
      let h = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: superview, attribute: .height, multiplier: 1, constant: 0)
      
      
      superview.addConstraints([x,y,w,h])
   }
}

class MethodHelper {
   private static var hud = JGProgressHUD(style: JGProgressHUDStyle.dark)
   
   class func showHudWithMessage (message:String, view:UIView) {
      self.hud?.textLabel.text = message
      self.hud?.show(in: view)
   }
   
   class func hideHUD () {
      self.hud?.dismiss()
   }
}
